

function draw() {
    if (window.requestAnimationFrame) window.requestAnimationFrame(draw);
    // IE implementation
    else if (window.msRequestAnimationFrame) window.msRequestAnimationFrame(draw);
    // Firefox implementation
    else if (window.mozRequestAnimationFrame) window.mozRequestAnimationFrame(draw);
    // Chrome implementation
    else if (window.webkitRequestAnimationFrame) window.webkitRequestAnimationFrame(draw);
    // Other browsers that do not yet support feature
    else setTimeout(draw, 16.7);

    //DrawVideoOnCanvas(110, 154, 90);

    DrawVideoOnCanvas( 
        [
            // [180,183,193],
            // [116,119,113],
            // [185,184,194],
            // [171,176,174],
            // [182,178,187],
            // [154,156,154],
            // [97,99,100],
            // [95,94,96],
            // [121,123,119],
            // [107,107,105],
            // [125,127,121],
            // [175,172,179]
            [232, 233, 242],
            [233,232,241],
            [128,159,144],
            [141,171,155],
            
            [132,160,146],
            [160,161,166],
            [187,96,81],
            [225,207,209]

        ]
    );
    //DrawVideoOnCanvas(35, 187, 48);
    //DrawVideoOnCanvas(57, 157, 50);
    //DrawVideoOnCanvas(30, 182, 29);
}

function getColorsFromImageData (imageData) { 
    var colors = {};
    var data = imageData.data;

    for (var i = 0, len = data.length; i < len; i += 4) {
        if (data[i+3] == 0) continue;
        var rgb = data[i]+","+data[i+1]+","+data[i+2];
        if (colors[rgb])
            colors[rgb]++;
        else
            colors[rgb] = 1;
    }
    return colors;
};

function createColorElement (color, count) {
    var elm = document.createElement("span");
    elm.style.backgroundColor = "rgb(" + color + ")";
    elm.className = "color";
    elm.innerHTML = color + " (" + count + ")";
    document.getElementById("getTHIS").appendChild(elm);
}
 


var color_to_except = [
    [170,141,128],
    [151,118,112]
];

var error_marge = 15;
var lower_error_marge = 5;
 
var dejacolor = 0;
var canvaa = 0;
function DrawVideoOnCanvas(ArrayOfColorToRemove) {
    var object = document.getElementById("videodata")
    var width = object.width;
    var height = object.height;
    var canvas = document.getElementById("videoscreen");
    canvas.setAttribute('width', width);
    canvas.setAttribute('height', height);
    if (canvas.getContext) {
        var context = canvas.getContext('2d');
        context.drawImage(object, 0, 0, width, height);
        imgDataNormal = context.getImageData(0, 0, width, height);
        var imgData = context.createImageData(width, height);

            //console.log(getAverageRGB(context));
            // if(dejacolor<=2){
            //     var colors = getColorsFromImageData(imgDataNormal);
                
            //     var count = 0;
            //     for (var color in colors) {
            //         createColorElement(color, colors[color]);
            //         count++;
            //     }
                
            //     document.getElementById("count").innerHTML = count;
            //     dejacolor++;
            // }

            for (i = 0; i < imgData.width * imgData.height * 4; i += 4) {

                var r = imgDataNormal.data[i + 0];
                var g = imgDataNormal.data[i + 1];
                var b = imgDataNormal.data[i + 2];
                var a = imgDataNormal.data[i + 3];
                // set rgb levels for green and set alphachannel to 0;
 
 
                    for (var canva = 0; canva < ArrayOfColorToRemove.length; canva++) {
                        var selectedR = ArrayOfColorToRemove[canva][0];
                        var selectedG = ArrayOfColorToRemove[canva][1];
                        var selectedB = ArrayOfColorToRemove[canva][2];
 
                        for (var ia = 0; ia < error_marge; ia++) {
                            if ((r <= selectedR+ia && g >= selectedG+ia && b >= selectedB+ia) || (r <= selectedR-ia && g >= selectedG-ia && b >= selectedB-ia)) {
                                a = 0;
                                break;
                            }
                            // else{
                            //     for (var ib = 0; ib < lower_error_marge; ib++) {
 
                            //         if (r <= selectedR+ib && g >= selectedG && b >= selectedB) {
                            //             a = 0;
                            //             break;
                            //         }else{
                            //             if (r <= selectedR && g >= selectedG+ib && b >= selectedB) {
                            //                 a = 0;
                            //                 break;
                            //             }else{
                            //                 if (r <= selectedR && g >= selectedG && b >= selectedB+ib) {
                            //                     a = 0;
                            //                     break;
                            //                 }
                            //             }
                            //         }
 
                            //     }
                            // }
                        }

                        if(a == 0){
                            a = 0;
                            break;
                        }
 
                        // else{ // Errormarge
                        //     for (var ia = 0; ia < error_marge; ia++) {
                        //         for (var ib = 0; ib < error_marge; ib++) {
                        //             for (var ic = 0; ic < error_marge; ic++) {
 
                        //                 if (r <= selectedR+ia && g >= selectedG+ib && b >= selectedB+ic) {
                        //                     a = 0;
                        //                     break;
                        //                 }

                        //             }
                        //                 if (r <= selectedR+ia && g >= selectedG+ib && b >= selectedB+ic) {
                        //                     a = 0;
                        //                     break;
                        //                 }
                        //                 if(a == 0){
                        //                     a = 0;
                        //                     break;
                        //                 }
                        //         }
                        //                 if (r <= selectedR+ia && g >= selectedG+ib && b >= selectedB+ic) {
                        //                     a = 0;
                        //                     break;
                        //                 }
                        //             if(a == 0){
                        //                 a = 0;
                        //                 break;
                        //             }
                        //     }
                        //                 if (r <= selectedR+ia && g >= selectedG+ib && b >= selectedB+ic) {
                        //                     a = 0;
                        //                     break;
                        //                 }
                        //         if(a == 0){
                        //             a = 0;
                        //             break;
                        //         }
                        // }
                    }

                    if (a != 0) {
                        imgData.data[i + 0] = r;
                        imgData.data[i + 1] = g;
                        imgData.data[i + 2] = b;
                        imgData.data[i + 3] = a;
                    }
 
            }
 
            // For image anti-aliasing
            for (var y = 0; y < imgData.height; y++) {
                for (var x = 0; x < imgData.width; x++) {
                    var r = imgData.data[((imgData.width * y) + x) * 4];
                    var g = imgData.data[((imgData.width * y) + x) * 4 + 1];
                    var b = imgData.data[((imgData.width * y) + x) * 4 + 2];
                    var a = imgData.data[((imgData.width * y) + x) * 4 + 3];
                    if (imgData.data[((imgData.width * y) + x) * 4 + 3] != 0) {
                        offsetYup = y - 1;
                        offsetYdown = y + 1;
                        offsetXleft = x - 1;
                        offsetxRight = x + 1;
                        var change = false;
                        if (offsetYup > 0) {
                            if (imgData.data[((imgData.width * (y - 1)) + (x)) * 4 + 3] == 0) {
                                change = true;
                            }
                        }
                        if (offsetYdown < imgData.height) {
                            if (imgData.data[((imgData.width * (y + 1)) + (x)) * 4 + 3] == 0) {
                                change = true;
                            }
                        }
                        if (offsetXleft > -1) {
                            if (imgData.data[((imgData.width * y) + (x - 1)) * 4 + 3] == 0) {
                                change = true;
                            }
                        }
                        if (offsetxRight < imgData.width) {
                            if (imgData.data[((imgData.width * y) + (x + 1)) * 4 + 3] == 0) {
                                change = true;
                            }
                        }
                    }
                }
            }

        context.putImageData(imgData, 0, 0);
    }
}
